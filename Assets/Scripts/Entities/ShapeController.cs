﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShapeController : MonoBehaviour
{
    private float rotationAcceleration = 0.01f;

    private float dampingRotationAcceleration = 0.01f;

    private float maxSpeed = 0.5f;

    private float rotationSpeed = 0;
    void Update()
    {

        if (!GameManager.Instance.GamePaused)
        {
            if (Input.GetAxis("Horizontal") > .1)
            {
                rotationSpeed += rotationAcceleration;
                rotationSpeed = Mathf.Min(rotationSpeed, maxSpeed);
            }
            else if (Input.GetAxis("Horizontal") <= -.1)
            {
                rotationSpeed -= rotationAcceleration;
                rotationSpeed = Mathf.Max(rotationSpeed, - maxSpeed);
            }
            else if(rotationSpeed > 0)
            {
                rotationSpeed -= dampingRotationAcceleration;
                rotationSpeed = Mathf.Max(rotationSpeed, 0);
            }
            else if (rotationSpeed < 0)
            {
                rotationSpeed += dampingRotationAcceleration;
                rotationSpeed = Mathf.Min(rotationSpeed, 0);
            }

            transform.Rotate(Vector3.back, rotationSpeed);
        }
    }
}
