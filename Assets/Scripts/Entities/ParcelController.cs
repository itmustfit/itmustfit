﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(SpriteRenderer))]
public class ParcelController : MonoBehaviour
{
    public float fallSpeed = 0.1f;

    private float rotationAcceleration = 0.01f;

    private float dampingRotationAcceleration = 0.01f;

    private float maxSpeed = 0.5f;

    private float rotationSpeed = 0;

    private bool anchoredToToy = false;

    private Parcel parcelData;

    void Update()
    {
        if(!anchoredToToy && !GameManager.Instance.GamePaused)
        {
            if (Input.GetAxis("Vertical") > .1)
            {
                rotationSpeed += rotationAcceleration;
                rotationSpeed = Mathf.Min(rotationSpeed, maxSpeed);
            }
            else if (Input.GetAxis("Vertical") <= -.1)
            {
                rotationSpeed -= rotationAcceleration;
                rotationSpeed = Mathf.Max(rotationSpeed, -maxSpeed);
            }
            else if (rotationSpeed > 0)
            {
                rotationSpeed -= dampingRotationAcceleration;
                rotationSpeed = Mathf.Max(rotationSpeed, 0);
            }
            else if (rotationSpeed < 0)
            {
                rotationSpeed += dampingRotationAcceleration;
                rotationSpeed = Mathf.Min(rotationSpeed, 0);
            }

            transform.Rotate(Vector3.back, rotationSpeed);

            transform.Translate(Vector3.down * fallSpeed, Space.World);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!anchoredToToy && collision.otherCollider.gameObject.layer == LayerMask.NameToLayer("Toy"))
        {
            AnchorToToy();

            GameManager.Instance.NextParcel();
        }
    }

    public void AnchorToToy()
    {
        anchoredToToy = true;

        Rigidbody2D rigidbody2D = GetComponent<Rigidbody2D>();

        rigidbody2D.constraints = RigidbodyConstraints2D.FreezeAll;
    }

    public void SetParcel(Parcel parcel)
    {
        parcelData = parcel;

        GetComponent<SpriteRenderer>().sprite = parcel.sprite;
        PolygonCollider2D collider = gameObject.AddComponent<PolygonCollider2D>();

        collider.usedByComposite = true;
        collider.autoTiling = true;
    }
}
