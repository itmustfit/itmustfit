﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ItMustFit/Parcel", order = 1)]
public class Parcel : ScriptableObject
{
    [SerializeField]
    public Sprite sprite;
}
