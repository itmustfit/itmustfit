﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ItMustFit/ToyDatabase", order = 3)]
public class ToyDatabase : ScriptableObject
{
    [SerializeField]
    public List<Toy> Toys;
}
