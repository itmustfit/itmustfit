﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ItMustFit/Toy", order = 2)]
public class Toy : ScriptableObject
{
    [SerializeField]
    public Sprite sprite;

    [SerializeField]
    public List<Parcel> parcels;

    [SerializeField]
    public Parcel anchorParcel;

    [SerializeField]
    public int difficulty;

    [SerializeField]
    public float minFitScore;

    [SerializeField]
    public float maxFitScore;
}
