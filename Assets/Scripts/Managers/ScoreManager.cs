﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class ScoreEvent : UnityEvent<int, int> { }

public class ScoreManager : Singleton<ScoreManager>
{
    [SerializeField]
    private float textureSizeRatio = 0.5f;
    public int OverallScore { get; private set; } = 0;
    public int OverallHighScore { get; private set; } = 0;

    private Dictionary<Toy, int> levelHighScores = new Dictionary<Toy, int>();

    private Dictionary<Toy, int> levelScores = new Dictionary<Toy, int>();

    public ScoreEvent OnScoreUpdated;
    public ScoreEvent OnOverallScoreUpdated;

    protected ScoreManager() { }

    private void Start()
    {
        Reset();
    }

    public void Reset()
    {
        OverallScore = 0;
        OverallHighScore = 0;
        levelHighScores.Clear();
        levelScores.Clear();
    }

    public int ComputeScore(RenderTexture gameRenderTexture, Toy currentToy)
    {
        Texture2D texture = new Texture2D(gameRenderTexture.width, gameRenderTexture.height, TextureFormat.RGBA32, false);
        RenderTexture.active = gameRenderTexture;
        texture.ReadPixels(new Rect(0, 0, gameRenderTexture.width, gameRenderTexture.height), 0, 0);
        texture.Apply();

        Texture2D toyTexture = currentToy.sprite.texture;

        int resultPixelNumber = 0;
        int toyPixelNumber = 0;

        foreach (Color32 color in texture.GetPixels32())
        {
            if (color.a < 250 && color.a > 5)
                resultPixelNumber++;
        }

        foreach (Color32 color in toyTexture.GetPixels32())
        {
            if (color.a > 5)
                toyPixelNumber++;
        }

        float result = 1.0f - ((float)resultPixelNumber * textureSizeRatio / (float)toyPixelNumber);
        result = Mathf.Clamp(result, currentToy.minFitScore, currentToy.maxFitScore);
        result = (result - currentToy.minFitScore) * 100f / (currentToy.maxFitScore - currentToy.minFitScore);

        levelScores[currentToy] = ((int)result) * 100;

        if (!levelHighScores.ContainsKey(currentToy) || levelScores[currentToy] > levelHighScores[currentToy])
            levelHighScores[currentToy] = levelScores[currentToy];

        OnScoreUpdated?.Invoke(levelScores[currentToy], levelHighScores[currentToy]);

        updateOverallScore();

        return (int) result;
    }

    public void updateOverallScore()
    {
        OverallScore = 0;

        foreach (KeyValuePair<Toy, int> score in levelScores)
        {
            OverallScore += score.Value;
        }

        if (OverallScore > OverallHighScore)
            OverallHighScore = OverallScore;

        OnOverallScoreUpdated?.Invoke(OverallScore, OverallHighScore);
    }
}
