﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : Singleton<LevelManager>
{
    [SerializeField]
    private int currentLevel = 0;

    public int CurrentLevel { get { return LevelManager.Instance.currentLevel; } set { LevelManager.Instance.currentLevel = value; } }

    [SerializeField]
    public ToyDatabase ToyDatabase;

    public int LevelCount => ToyDatabase.Toys.Count;

    protected LevelManager() { }
    private void Start()
    {
        Reset();
    }

    public void Reset()
    {
        currentLevel = 0;
    }

    public Toy GetCurrentToy()
    {
        if (CurrentLevel >= ToyDatabase.Toys.Count)
        {
            CurrentLevel = 0;
        }

        return ToyDatabase.Toys[CurrentLevel];
    }
}
