﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManager : Singleton<SceneManager>
{
    private Scene previousScene;
    private bool isThereAPreviousScene = false;

    private SceneData sceneToLoad = null;

    [SerializeField]
    private SceneData MenuSceneData;
    [SerializeField]
    private SceneData GameSceneData;

    [SerializeField]
    private AudioManager audioManager;
    protected SceneManager() { }

    // Start is called before the first frame update
    void Start()
    {
        LoadMenu();
    }

    void OnEnable()
    {
        UnityEngine.SceneManagement.SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnDisable()
    {
        UnityEngine.SceneManagement.SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    public void LoadMenu()
    {
        LoadScene(MenuSceneData);
    }

    public void LoadLevel()
    {
        LoadScene(GameSceneData);
    }

    public static void LoadScene(SceneData sceneData)
    {
        SceneManager.Instance.sceneToLoad = sceneData;

        var parameters = new LoadSceneParameters(LoadSceneMode.Additive);
        UnityEngine.SceneManagement.SceneManager.LoadScene(sceneData.sceneName, parameters);
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (mode != LoadSceneMode.Additive)
            return;

        if(sceneToLoad)
            audioManager.FadeTo(sceneToLoad.music);

        if (isThereAPreviousScene)
            UnityEngine.SceneManagement.SceneManager.UnloadSceneAsync(previousScene);
        else
            isThereAPreviousScene = true;

        previousScene = scene;
    }
}
