﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.IO;

public class GameManager : Singleton<GameManager>
{
    [SerializeField]
    private LevelManager levelManager;

    [SerializeField]
    private ScoreManager scoreManager;

    [SerializeField]
    private SceneManager sceneManager;

    [SerializeField]
    private Transform parcelAnchor;

    [SerializeField]
    private Transform toyAnchor;

    [SerializeField]
    private ParcelController parcelPrefab;

    [SerializeField]
    private GameObject patternPrefab;

    [SerializeField]
    private RenderTexture gameRenderTexture;

    public bool GamePaused { get; private set; } = false;

    private Toy currentToy;

    private int currentParcelNumber = -1;

    [SerializeField]
    public UnityEvent OnGameOver;

    [SerializeField]
    public UnityEvent OnWin;

    [SerializeField]
    public UnityEvent OnLose;

    [SerializeField]
    public ScoreEvent OnScoreUpdated;

    [SerializeField]
    public ScoreEvent OnOverallScoreUpdated;

    private void Start()
    {
        scoreManager.OnScoreUpdated.AddListener((score, highscore) => OnScoreUpdated?.Invoke(score, highscore));
        scoreManager.OnOverallScoreUpdated.AddListener((score, highscore) => OnOverallScoreUpdated?.Invoke(score, highscore));

        //GenerateLevel();
    }

    public void NextParcel()
    {
        if (parcelAnchor.childCount > 0)
        {
            Transform parcelObj = parcelAnchor.GetChild(0);

            parcelObj.SetParent(toyAnchor, true);
        }

        if (++currentParcelNumber < currentToy.parcels.Count)
        {
            ParcelController parcelController = Object.Instantiate(parcelPrefab, parcelAnchor);

            parcelController.SetParcel(currentToy.parcels[currentParcelNumber]);
            parcelController.transform.Rotate(Vector3.back, Random.Range(0f, 360f));
        }
        else
            CheckLevelResult();
    }

    public void CheckLevelResult()
    {
        if (scoreManager.ComputeScore(gameRenderTexture, currentToy) > 50)
        {
            OnWin?.Invoke();
        }
        else
        {
            OnLose?.Invoke();
        }
    }

    public void NextLevel()
    {
        if (++levelManager.CurrentLevel < levelManager.LevelCount)
        {
            GenerateLevel();
        }
        else
        {
            OnGameOver?.Invoke();
        }
    }

    public void GenerateLevel()
    {
        currentToy = levelManager.GetCurrentToy();
        currentParcelNumber = -1;

        GameObject currentObj;

        while (toyAnchor.childCount > 0)
        {
            currentObj = toyAnchor.GetChild(0).gameObject;
            currentObj.SetActive(false);
            currentObj.transform.SetParent(currentObj.transform.root);
            GameObject.Destroy(currentObj);
        }

        while (parcelAnchor.childCount > 0)
        {
            currentObj = parcelAnchor.GetChild(0).gameObject;
            currentObj.SetActive(false);
            currentObj.transform.SetParent(currentObj.transform.root);
            GameObject.Destroy(currentObj);
        }

        GameObject pattern = Object.Instantiate(patternPrefab, toyAnchor);

        pattern.GetComponent<SpriteRenderer>().sprite = currentToy.sprite;

        ParcelController parcelController = Object.Instantiate(parcelPrefab, toyAnchor);

        parcelController.SetParcel(currentToy.anchorParcel);
        parcelController.AnchorToToy();

        NextParcel();
    }

    public void PauseGame()
    {
        Time.timeScale = 0;
        GamePaused = true;
    }

    public void ResumeGame()
    {
        Time.timeScale = 1;
        GamePaused = false;
    }

    public void StopGame()
    {
        levelManager.Reset();
        scoreManager.Reset();
        sceneManager.LoadMenu();
    }
}
