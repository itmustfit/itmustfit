﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using System.Threading.Tasks;

public class AudioManager : Singleton<AudioManager>
{
    [SerializeField]
    private AudioMixer mixer;

    [SerializeField]
    private AudioSource currentMusicSource;

    [SerializeField]
    private AudioSource newMusicSource;
    protected AudioManager() { }
    public static IEnumerator FadeOut(AudioSource audioSource, float FadeTime)
    {
        float startVolume = audioSource.volume;

        while (audioSource.volume > 0)
        {
            audioSource.volume -= startVolume * Time.deltaTime / FadeTime;

            yield return new WaitForUpdate();
        }

        audioSource.Stop();
        audioSource.volume = startVolume;
    }

    public static IEnumerator FadeIn(AudioSource audioSource, float FadeTime, float endVolume = 0)
    {
        if (endVolume == 0)
            endVolume = audioSource.volume;

        audioSource.volume = 0;
        audioSource.Play();

        while (audioSource.volume < endVolume)
        {
            audioSource.volume += endVolume * Time.deltaTime / FadeTime;

            yield return new WaitForUpdate();   
        }

        audioSource.volume = endVolume;
    }

    public async void FadeTo(AudioClip audioClip, float FadeTime = 1.0f)
    {
        newMusicSource.clip = audioClip;

        Task fadeOut = Task.Run(async () => await FadeOut(currentMusicSource, FadeTime));
        Task fadeIn = Task.Run(async () => await FadeIn(newMusicSource, FadeTime));

        await Task.WhenAll(fadeOut, fadeIn);

        AudioSource tempSource = currentMusicSource;
        currentMusicSource = newMusicSource;
        newMusicSource = tempSource;
    }
}
