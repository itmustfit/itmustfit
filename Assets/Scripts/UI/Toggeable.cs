﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Toggeable : MonoBehaviour
{
    public GameObject BeginScreen;
    public GameObject LooseScreen;
    public GameObject WinScreen;
    public GameObject FinishScreen;

    // Start is called before the first frame update
    void Start()
    {
        
        

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void showBegin()
    {
        BeginScreen.SetActive(true);
    }

    public void showWin()
    {
        WinScreen.SetActive(true);
    }

    public void showLoose()
    {
        LooseScreen.SetActive(true);
    }

    public void showFinish()
    {
        FinishScreen.SetActive(true);
    }

    public void closeBegin()
    {
        BeginScreen.SetActive(false);
    }

    public void closeWin()
    {
        WinScreen.SetActive(false);
    }

    public void closeLoose()
    {
        LooseScreen.SetActive(false);
    }

    public void closeFinish()
    {
        FinishScreen.SetActive(false);
    }
}
