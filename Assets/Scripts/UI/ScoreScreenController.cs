﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

class ScoreScreenController : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI ScoreField;

    [SerializeField]
    private TextMeshProUGUI HighScoreField;

    public bool isOverallScore = false;

    public void UpdateScores(int score, int highscore)
    {
        if (ScoreField != null)
            ScoreField.text = score.ToString();


        if (HighScoreField != null)
            HighScoreField.text = highscore.ToString();
    }
}
