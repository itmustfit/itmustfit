﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Toggleable : MonoBehaviour
{
    [SerializeField]
    private bool activated;

    [SerializeField]
    public GameObject target;

    [SerializeField]
    private bool animated;

    [SerializeField]
    private bool animateOnStart;

    [SerializeField]
    private Animation AnimationIn;

    [SerializeField]
    private Animation AnimationOut;

    private bool toggleOnOnAnimationFinished = false;
    private bool toggleOffOnAnimationFinished = false;

    void Start()
    {
        if (target != null)
        {
            if (activated)
                ToggleOn(animateOnStart);
            else
                ToggleOff(animateOnStart);
        }
    }

    private void Update()
    {
        if (toggleOnOnAnimationFinished)
        {
            if(AnimationIn && !AnimationIn.isPlaying)
            {
                toggleOnOnAnimationFinished = false;
            }
        }

        if (toggleOffOnAnimationFinished)
        {
            if (AnimationOut && !AnimationOut.isPlaying)
            {
                toggleOffOnAnimationFinished = false;
                target.SetActive(false);
            }
        }
    }

    public void Toggle(bool? active = null, bool animate = true)
    {
        bool hasChanged = false;

        if (active == null || activated != active)
        {
            activated = !activated;
            hasChanged = true;
        }

        if(hasChanged)
        {
            if (activated)
                ToggleOn(animate);
            else
                ToggleOff(animate);
        }
    }

    public void ToggleOn(bool animate = true)
    {
        if(animate && animated && AnimationIn)
        {
            target.SetActive(true);
            AnimationIn.Play();

            toggleOnOnAnimationFinished = true;
        }
        else
        {
            target.SetActive(true);
        }
    }

    public void ToggleOff(bool animate = true)
    {
        if (animate && animated && AnimationOut)
        {
            AnimationOut.Play();

            toggleOffOnAnimationFinished = true;
        }
        else
        {
            target.SetActive(false);
        }
    }
}
