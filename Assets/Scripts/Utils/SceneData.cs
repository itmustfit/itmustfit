﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu(fileName = "Data", menuName = "ItMustFit/SceneData", order = 4)]
public class SceneData : ScriptableObject
{
    [SerializeField]
    public string sceneName;

    [SerializeField]
    public AudioClip music;
}
